#Création d'une plateforme de partage entre Professeur et élèves:

Cette application contiendra une diverses connexion :

1- Connexion Administrateur :
 Celui ci aura la possibilité de :
- Consulter la liste des adhérents
- Consulter la liste des élèves (date d'entrée, de sorti, école ou le ou les cours qu'ils suivent se trouve, instrument joué, horaire des cours...)
- Consulter la liste des parent
- Consulter la liste des écoles
- Consulter la liste des instruments en cours de pres au élèves
- Ajouter des documents
- Ajouter des évènements
- Supprimer des élèves/parents/prof/adhérent
- Modifier le statut des adhérants au sein de l'application (admin/prof/eleves)
- Consulter les documents associatifs (licence...)

2- Connexion Parent :
 Celui ci aura la possibilité de :
- Suivre les appréciations des professeur sur son enfant (pour les élèves mineurs)
- Accéder à sa propre méssagerie et ainsi communiquer avec les professeur
- Déclarer au professeur un retard ou une absence de son enfant
- Consulter les documents relatif au règlement de l'école, au prêt des instruments et à l'inscription)
- Consulter les évènements à venir
- Consulter les horaires de cours de son enfant

3- Connexion Elève :
 Celui ci aura la possibilité de :
- Consulter ses cours (horaires, professeur concerné)
- Accéder à sa propre messagerie et ainsi communiquer avec les professeur et ses camarades
- Consulter les évènements à venir
- Déclarer au professeur un retard ou une absence (pour les adultes)
- S'enregistrer lors des entrainements à la maison 
- Prendre des notes sur le suivis de sa progression ou des choses à ne pas oublier
- Consulter ses partitions mise en ligne par le professeur

4- Connexion professeur :
 Celui ci aura la possibilité de :
- Consulter son planning et le mettre en ligne
- Accéder à sa propre messagerie et ainsi communiquer avec les parents, élèves et ses collègues
- Consulter les évènements à venir
- Mettre en ligne les partitions pour les élèves
- Mettre des appréciations au élèves destiné au parents des mineurs
- Mettre des notes pour les suivi de cours (rythme, note, chanson...)
- Consulter et mettre en ligne des documents



