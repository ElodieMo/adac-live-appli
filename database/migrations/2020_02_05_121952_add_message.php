<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('from_id')->unsigned(); //unsigne = pas de signe négatif//
            $table->integer('to_id')->unsigned();
            //delete cascade permettra de supprimer les messages associés à l'utilisateur lors de la suppression de celui ci//
            $table->foreign('from_id')->name ('from')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('to_id')->name ('to')->references('id')->on('users')->onDelete('cascade');
            $table->text('content');
            $table->timestamp('created_at')->useCurrent();//grace à useCurrent, la date de creation se mettra automatiquement//
            $table->dateTime('read_at')->nullable(); //nullable car par defaut un message ne sera pas lu//
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
