<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cours')->insert([
            [
                'name' => 'ADAC Live',
            ],
            [
                'name' => 'atelier',
            ],
            [
                'name' => 'atelier Est musique',
            ],
            [
                'name' => 'atelier accordéon diato',
            ],
            [
                'name' => 'atelier accordéons',
            ],
            [
                'name' => 'atelier bluegrass/irlanda',
            ],
            [
                'name' => 'atelier chant ados',
            ],
            [
                'name' => 'atelier chant adultes',
            ],
            [
                'name' => 'atelier chant enfants',
            ],
            [
                'name' => 'atelier clavier',
            ],
            [
                'name' => 'atelier cordes',
            ],
            [
                'name' => 'atelier flûte',
            ],
            [
                'name' => 'atelier guitare',
            ],
            [
                'name' => 'atelier Impro.',
            ],
            [
                'name' => 'atelier jazz',
            ],
            [
                'name' => 'atelier musique chambre',
            ],
            [
                'name' => 'atelier parent/enfant',
            ],
            [
                'name' => 'atelier rock',
            ],
            [
                'name' => 'atelier trad.',
            ],
            [
                'name' => 'atelier trompette',
            ],
            [
                'name' => 'atelier variétés',
            ],
            [
                'name' => 'atelier vents',
            ],
            [
                'name' => 'atelier violon',
            ],
            [
                'name' => 'atelier violoncelle',
            ],
            [
                'name' => "Buddy's Team",
            ],
            [
                'name' => 'chant',
            ],
            [
                'name' => 'chant groupe',
            ],
            [
                'name' => 'chant/détente',
            ],
            [
                'name' => 'danse',
            ],
            [
                'name' => 'ensemble',
            ],
            [
                'name' => 'ensemble accordéon',
            ],
            [
                'name' => 'études duo/trio',
            ],
            [
                'name' => 'éveil école',
            ],
            [
                'name' => 'éveil musical',
            ],
            [
                'name' => "FANFAR'ADAC",
            ],
            [
                'name' => 'instrument',
            ],
            [
                'name' => 'Music Rock ADAC',
            ],
            [
                'name' => 'orchestre',
            ],
            [
                'name' => 'percussions',
            ],
            [
                'name' => 'solfège',
            ],
            [
                'name' => 'théâtre',
            ],
        ]);

    }
}
