<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(InstrumentsTableSeeder::class);
        $this->call(EcolesTableSeeder::class);
        $this->call(CoursTableSeeder::class);
    }
}
