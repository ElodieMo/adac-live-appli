<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EcolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ecoles')->insert([
        [
            'name' => 'Adac',
            'lieu' => 'Adac',
        ],
        [
            'name' => 'Boulogne',
            'lieu' => 'Boulogne Sur Gesse',
        ],
        [
            'name' => 'Castelnau',
            'lieu' => 'Castelnau Magnoac',
        ],
        [
            'name' => 'Aidam',
            'lieu' => 'A.I.D.A.M',
        ],
        [
            'name' => 'Clarac',
            'lieu' => 'Clarac',
        ],
        [
            'name' => 'Gourdan',
            'lieu' => 'Gourdan Polignan',
        ],
        [
            'name' => 'Larroque',
            'lieu' => 'Larroque',
        ],
        [
            'name' => 'Loures',
            'lieu' => "Vincent D'indy",
        ],
        [
            'name' => "L'isle",
            'lieu' => "L'isle En Dodon",
        ],
        [
            'name' => 'Pointis',
            'lieu' => 'Pointis Inard',
        ],
        [
            'name' => 'Théâtre Clarac',
            'lieu' => 'Théâtre Clarac',
        ],
        [
            'name' => 'Théâtre Boulogne',
            'lieu' => 'Théâtre Boulogne',
        ],
        [
            'name' => 'Ste Anne',
            'lieu' => 'Ecole Ste Anne',
        ],
        [
            'name' => 'Adac Percussion',
            'lieu' => 'Adac percussion',
        ],
        [
            'name' => 'Montréjeau',
            'lieu' => 'Montréjeau',
        ],
        [
            'name' => 'Boulogne Écoles',
            'lieu' => 'Boulogne',
        ],
        [
            'name' => 'Montréjeau Écoles',
            'lieu' => 'Montréjeau',
        ],
        [
            'name' => 'Boulogne Collège',
            'lieu' => 'Boulogne',
        ],
        [
            'name' => 'Loures Écoles',
            'lieu' => 'Loures',
        ],
        [
            'name' => 'Ste Therese',
            'lieu' => 'St Gaudens',
        ],
        [
            'name' => 'Crêche Carabistouille',
            'lieu' => 'Montréjeau',
        ],
        [
            'name' => 'Gourdan Écoles',
            'lieu' => 'Gourdan Polignan',
        ],
        [
            'name' => 'ENCAUSSE',
            'lieu' => 'Encausse-Les-Thermes',
        ],
        [
            'name' => 'St Bertrand',
            'lieu' => 'Saint-Bertrand-de-Comminges',
        ],
        [
            'name' => 'Larroque(FR)',
            'lieu' => 'Larroque(FR)',
        ],
    ]);   
}
}