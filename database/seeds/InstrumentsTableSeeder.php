<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Instrument;

class InstrumentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('instruments')->insert([
        'name'=> 'Accordéon',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Accordéon diatonique',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Basse',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Basse si b',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Batterie',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Chant',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Chant leads',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Clairon',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Clarinette',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Congas',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Contrebasse',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Cor',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Cornemuse',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Danse',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Euphonium',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Flûte',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Flûte à bec',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Flûte alto',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Flûte soprano',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Flûte traversière',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Guitare',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Guitare éléctrique',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Guitare sèche',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Harpe celtique',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Mandoline',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Orgue',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Percussions',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Piano',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Présentation',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Rhodes',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Sax alto',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Sax baryton',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Sax soprano',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Sax ténor',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Saxophone',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Synthétiseur',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Technique',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Trombone',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Trompette',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Trompette à coulisse',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Trompette Si bémol',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Violon',
        ]);
        DB::table('instruments')->insert([
        'name'=> 'Violoncelle',
        ]);
    }
}
