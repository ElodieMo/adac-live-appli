<?php

use App\Role;
use Illuminate\Database\Seeder;


class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();

        Role::create(['name'=> 'admin']);
        Role::create(['name'=> 'professeur']);
        Role::create(['name'=> 'parent']);
        Role::create(['name'=> 'eleve']);
        Role::create(['name'=> 'admin_membre_conseil']);

    }
}
