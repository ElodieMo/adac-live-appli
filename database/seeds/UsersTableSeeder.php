<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        DB::table('role_user')->truncate();

        $admin = User::create([
            'name' => 'admin',
            'surname' => 'administrateur',
            'email'=> 'admin@admin.com',
            'password' => Hash::make('secret')
        ]);

        $admin = User::create([
            'name' => 'admin',
            'surname' => 'elodie',
            'email'=> 'elodie.morisan@hotmail.fr',
            'password' => Hash::make('secret')
        ]);

        $professeur = User::create([
            'name' => 'professeur',
            'surname' => 'prof',
            'email'=> 'professeur@professeur.com',
            'password' => Hash::make('secret')
        ]);

        $parent = User::create([
            'name' => 'parent',
            'surname' => 'parent',
            'email'=> 'parent@parent.com',
            'password' => Hash::make('secret')
        ]);

        $eleve = User::create([
            'name' => 'eleve',
            'surname' => 'eleve',
            'email'=> 'eleve@eleve.com',
            'password' => Hash::make('secret')
        ]);

        $admin_membre_conseil = User::create([
            'name' => 'admin_membre_conseil',
            'surname'=> 'membre_conseil',
            'email'=> 'membre@conseil.com',
            'password' => Hash::make('secret')
        ]);


        $adminRole = Role::where('name', 'admin')->first();
        $professeurRole = Role::where('name', 'professeur')->first();
        $parentRole = Role::where('name', 'parent')->first();
        $eleveRole = Role::where('name', 'eleve')->first();
        $admin_membre_conseilRole = Role::where('name', 'admin_membre_conseil')->first();

        $admin->roles()->attach($adminRole);
        $professeur->roles()->attach($professeurRole);
        $parent->roles()->attach($parentRole);
        $eleve->roles()->attach($eleveRole);
        $admin_membre_conseil->roles()->attach($admin_membre_conseilRole);

    }
}
