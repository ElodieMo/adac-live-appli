@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-64 h-15 cursor-default" type="text" disabled="disabled" name="mon profil">Mon profil</button>
</div>
<div class="-m-2 flex justify-center p-12 text-2xl text-black font-extrabold">Statut :</div>
<div class="flex flex-col items-center">
        <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="statut">{{ Auth::user()->roles->pluck('name') }}</button>
</div>
@if (Auth::user()->roles->pluck('name')->contains('admin'))
<div class="flex justify-center p-12 text-2xl text-black font-extrabold">Prénom :</div>
<div class="flex flex-col items-center">
    <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="Prénom">{{ Auth::user()->surname }}</button>
</div>
@endif
@if (Auth::user()->roles->pluck('name')->contains('professeur'))
<div class="flex justify-center p-12 text-2xl text-black font-extrabold">Prénom :</div>
<div class="flex flex-col items-center">
    <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="Prénom">{{ Auth::user()->surname }}</button>
</div>
<div class="flex justify-center p-12 text-2xl text-black font-extrabold">Cours :</div>
<div class="flex flex-col items-center">
    <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="cours">#</button>
</div>
@endif
@if (Auth::user()->roles->pluck('name')->contains('admin_membre_conseil'))
<div class="flex justify-center p-12 text-2xl text-black font-extrabold">Prénom :</div>
<div class="flex flex-col items-center">
    <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="Prénom">{{ Auth::user()->surname }}</button>
</div>
@endif
@if (Auth::user()->roles->pluck('name')->contains('eleve'))
<div class="flex justify-center p-12 text-2xl text-black font-extrabold">Prénom :</div>
<div class="flex flex-col items-center">
    <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="Prénom">{{ Auth::user()->surname }}</button>
</div>
@endif
@if (Auth::user()->roles->pluck('name')->contains('parent'))
<div class="flex justify-center p-12 text-2xl text-black font-extrabold">Prénom de l'enfant :</div>
<div class="flex flex-col items-center">
    <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="Prénom enfant">{{ Auth::user()->name }}</button>
</div>
@endif
<div class="flex justify-center p-12 text-2xl text-black font-extrabold">E-mail :</div>
    <div class="flex flex-col items-center">
        <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-auto cursor-default" disabled="disabled" type="button" name="email">{{ Auth::user()->email }}</button>
    </div>
    <!--<div class="flex justify-center p-12 text-2xl text-black font-extrabold">Numéro de téléphone :
    </div>
    <div class="flex flex-col items-center">
        <button class="-m-6 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15 cursor-default" disabled="disabled" type="button" name="telephone">00.00.00.00.00</button>
    </div>-->
        <a href="{{route('logout')}}" class="flex flex-col items-center hover:no-underline">
            <button class="flex justify-center mt-10 bg-red-800 text-white text-xl font-bold py-2 px-4 rounded-full w-54 h-15" >
                Déconnexion
            </button>
        </a>
    </div>
@endsection