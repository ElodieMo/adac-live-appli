<!--Navbar-->

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
<!-- Styles -->
<style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
}

    .full-height {
        height: 100vh;
}

    .flex-center {
        
        display: flex;
        justify-content: center;
}

    .position-ref {
        position: relative;
}

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
}

    .content {
        text-align: center;
}

    .title {
        font-size: 40px;
}

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 13px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
}

    .m-b-md {
        margin-bottom: 30px;
}
    .fa-home {
    color: white;
}
    .fa-user-circle {
    color: white;
}
</style>
<nav class="bg-indigo-900 h-30">
    <img src={{url('/img/logo.png')}}>
@if(Auth::check())
    <div class="absolute top-0 right-0 mt-10 m-8">
        @if (Auth::user()->roles->pluck('name')->contains('admin'))
        <a href="/mon-profiladmin"><i class="fas fa-user-circle fa-3x mr-2"></i></a>
        <a href="/bonjour-admin"><i class="fas fa-home fa-3x"></i></a></div>
        @endif
        @if (Auth::user()->roles->pluck('name')->contains('professeur'))
        <a href="/mon-profil"><i class="fas fa-user-circle fa-3x mr-2"></i></a>
        <a href="/bonjour-professeur"><i class="fas fa-home fa-3x"></i></a></div>
        @endif
        @if (Auth::user()->roles->pluck('name')->contains('parent'))
        <a href="/mon-profil"><i class="fas fa-user-circle fa-3x mr-2"></i></a>
        <a href="/bonjour-parent"><i class="fas fa-home fa-3x"></i></a></div>
        @endif
        @if (Auth::user()->roles->pluck('name')->contains('eleve'))
        <a href="/mon-profil"><i class="fas fa-user-circle fa-3x mr-2"></i></a>
        <a href="/bonjoureleve"><i class="fas fa-home fa-3x"></i></a></div>
        @endif
        @if (Auth::user()->roles->pluck('name')->contains('admin_membre_conseil'))
        <a href="/mon-profil"><i class="fas fa-user-circle fa-3x mr-2"></i></a>
        <a href="/bonjoureleve"><i class="fas fa-home fa-3x"></i></a></div>
        @endif
@endif
    <div class="flex-center">
    <div class="-mt-16 text-3xl text-white font-bold cursor-default bottom-0">ADAC</div>
    </div>
</nav>
<!-- Fin NavBar-->