<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
        <title>ADAC</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 40px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <nav class="bg-indigo-900 p-1">
            <img src="img/logo.png">
            <div class="flex-center">
            <div class="text-4xl text-white font-bold cursor-default bottom-0">ADAC</div>
            </div>
        </nav>
            <div class="flex flex-col items-center">
                <button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15 cursor-default" type="button" disabled="disabled">Connexion</button></div>
                <div class="flex justify-center p-8 text-2xl text-black font-extrabold">E-mail</div>
            <div class="flex justify-center p-3"><input class="bg-gray-300 h-10 w-56" type="email" id="email" name="email" pattern=".+@globex.com" size="30" required></div>
            <div class="flex justify-center p-8 text-2xl text-black font-extrabold">Mot de passe</div>
            <div class="flex justify-center p-3"><input class="bg-gray-300 h-10 w-56" type="password" id="pass" name="password" minlength="8" require></div>
            <div class="flex justify-center p-8 text-xl text-black font-extrabold underline">Mot de passe oublié</div>
            <div class="flex flex-col items-center">
                <a href="bonjour"><button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-56 h-15" type="text">C'est parti !</button></a>
            </div>
        </div>
        <<div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body>
</html>
