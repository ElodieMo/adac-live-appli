@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
@include('include.bouton-bonjour')
        <div class="flex flex-col items-center">
        <a href="/mon-profiladmin"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Mon profil</button></a>
        <div class="flex flex-col items-center">
        <a class="dropdowm-item flex flex-col items-center hover:no-underline" href="{{ route("admin.users.index") }}">
    <button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-15" >
            Gestion des utilisateurs
    </button>
        <div class="flex flex-col items-center">
        <a href="/gestionecoles"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-15" type="button">Gestion des écoles</button></a>
        </div>
        <div class="flex flex-col items-center">
                <a href="/gestioneleves"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-15" type="button">Gestion des élèves</button></a>
        </div>
        <div class="flex flex-col items-center">
                <a href="/gestioninstruments"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-15" type="button">Gestion des instruments</button></a>
        </div>
        <div class="flex flex-col items-center">
                <a href="/les-evenements"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-15" type="button">Les évènements</button></a>
        </div>
@endsection