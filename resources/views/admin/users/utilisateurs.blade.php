@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Liste des utilisateurs</button></div>
    <tbody class="flex-1 sm:flex-none">
    <div class="flex items-center justify-center">
        <div class="container">
    <table class="mt-8 text-black w-full flex flex-row flex-no-wrap sm:bg-white rounded-lg overflow-hidden sm:shadow-lg my-5">
        <thead class="text-white">
            @foreach ($users as $user)
            <tr class="bg-blue-900 flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                <th class="p-3 text-left">#</th>
                <th class="p-3 text-left">Nom</th>
                <th class="p-3 text-left">Email</th>
                <th class="p-3 text-left">Rôles</th>
                <th class="p-3 text-left py-4" width="110px">Action</th>
            </tr>
            @endforeach
        </thead>
            @foreach ($users as $user)
                <tr class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0">
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ $user->id }}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ $user->surname}}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3 truncate">{{ $user->email}}</td>
                    <td class="border-grey-light border hover:bg-gray-100 p-3">{{ implode(', ', $user->roles()->get()->pluck('name')->toArray()) }}</td>

                    <td class="border-grey-light border hover:bg-gray-100 text-red-400 hover:text-red-600 hover:font-medium cursor-pointer p-3">
                        <div class="flex mt-1">
                        <a href="{{ route('admin.users.edit', $user->id)}}">
                            <button type="submit" >
                                <i class="far fa-edit"></i>
                            </button>
                        </a>

                        <form action="{{ route ('admin.users.destroy',$user->id)}}" method="POST" class="ml-5">
                            @csrf
                            @method('DELETE')
                            <button type="submit" onclick="return confirm('Voulez vous supprimer ce profil ?')">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </form>
                        </div>
                    </td>
                </tr>
            </div>
            @endforeach
        </tbody>
    </table> 
@endsection
<style>
    html,
    body {
        height: 100%;
    }

    @media (min-width: 640px) {
    table {
        display: inline-table !important;
    }

    thead tr:not(:first-child) {
        display: none;
    }
    }

    td:not(:last-child) {
        border-bottom: 0;
    }

    th:not(:last-child) {
        border-bottom: 2px solid rgba(0, 0, 0, .1);
    }

</style>