@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Modifier <strong>{{ $user->name}}</strong></button>
</div>
<div class="flex flex-col items-center">
        <form action="{{ route('admin.users.update', $user) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-group row mt-8">
                <label for="email" class="cold-md-6 col-form-label">{{__('Adresse mail') }}</label>
                <div class="col-md-12">
                    <input id="email" type="email" class="form-control 
                    
                    @error('email') is-invalid 
                    
                    @enderror
                    "name="email" value="{{ old('email') ?? $user->email }}" requiere autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <label for="name" class="cold-md-6 col-form-label">{{__('Nom') }}</label>

                <div class="col-md-12">
                    <input id="name" type="text" class="form-control 
                    
                    @error('name') is-invalid 
                    @enderror"
                    name="name" value="{{ old('name') ?? $user->name }}" requiered autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            <div class="flex flex-col items-center">
            @foreach ($roles as $role)
                <div class="form-group form-check mt-8">
                    <input type="checkbox" class="form-check-input" name="roles[]" 
                    value="{{ $role->id }}" id="{{ $role->id }}" 
                    @if($user->roles->pluck('id')->contains($role->id))checked
                    @endif>
                    <label for ="{{ $role->id }}" class="form-check-label">{{ $role->name }}</label>
                </div>
            @endforeach
            </div>
        <button type="submit" class="m-2 mt-0 bg-blue-900 text-white text-xl font-bold py-1 px-3 rounded-full w-auto h-auto">Modifier les informations</button>
        </form>
</div>
@endsection