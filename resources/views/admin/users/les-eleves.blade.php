@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Liste des élèves</button>
    <table class="mt-8 table-auto text-black">
        <thead>
            <tr>
            <th class="px-4 py-2">#</th>
            <th class="px-4 py-2">Nom</th>
            <th class="px-4 py-2">Mail</th>
            </tr>
        </thead>
        <tbody>
            
            <tr>
            <td class="border px-4 py-4"></td>
            <td class="border px-4 py-4"></td>
            <td class="border px-4 py-4"></td>
            <td class="border px-4 py-4"></td>
            <td>
            <a href="#"><button class="m-2 mt-0 bg-blue-900 text-white text-xl font-bold py-1 px-3 rounded-full w-auto h-auto" type="text">Editer</button></a>
            <form action="#" method="POST" class="d-inline">
                @csrf
                @method('DELETE')
                <button type="submit" class="m-2 mt-0 bg-red-800 text-white text-xl font-bold py-1 px-2 rounded-full w-auto h-auto">Supprimer</button>
            </form>
            </td>
            </tr>
            
        </tbody>
        </table> 
</div>
@endsection