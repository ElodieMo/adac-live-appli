@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Liste des évènements</button></div>
</div>
@if (Auth::user()->roles->pluck('name')->contains('admin_membre_conseil'))
        <div class="flex flex-col items-center">
            <a href="{{route('events.create')}}"><button class="mt-8 bg-blue-900 text-white text-xl font-bold py-1 px-3 rounded-full w-auto h-auto"type="text">
            Créer un évènement
        </button></a>
        </div>   
@endif
<div class="flex items-center justify-center ml-auto mt-10">
        <div class="flex flex-wrap mx-2">
        @foreach ($events as $event)
            <div class="flex flex-wrap -mx-1 lg:-mx-4">
            <div class="flex -mx-3">
            <div class="px-6 py-0">
            <table class="w-full mt-8 text-black flex flex-row flex-no-wrap sm:bg-white rounded-lg overflow-hidden sm:shadow-lg my-5">
        <div class="max-w-sm rounded overflow-hidden shadow-lg ml-auto m-3">
            <img class="w-full mb-0" src="{{ asset('/storage') . '/' . $event->image }}" class="w-30" alt="{{ $event->name }}">
            <div class="py-4">
            <div class="font-bold text-xl mb-0 text-center"><h1>{{ $event->name }}</h1></div>
            <p class="text-gray-700 text-base text-center">
                {{ $event->caption }}
            </p>
            </div>
            <div class="py-4 flex items-center justify-center">
            <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">{{ $event->lieu }}</span>
            <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">{{ $event->date_event }}</span>
            <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2">{{ $event->heure_event }}</span>
            </div>
            @if (Auth::user()->roles->pluck('name')->contains('admin_membre_conseil'))
            <div class="flex flex-col items-center">
            <form action="{{ route ('events.destroy',$event->id)}}" method="POST" class="ml-5">
                @csrf
                @method('POST')
                <!--{{ method_field('delete') }}-->
                <button type="submit" onclick="return confirm('Voulez vous supprimer cet évènement ?')">
                    <i class="fas fa-trash-alt"></i>
                </button>
            </form>
            </div>
            @endif
            </table>
                </div>
            </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
    </div>
@endsection
