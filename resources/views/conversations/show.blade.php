@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
    <div class="container">
        <div class="row">
        @include('conversations.users', ['users' => $users])
        <div class="col-md-9">
            <div class="card">
        <div class="card-header">{{ $user->surname }}</div>
        <div class="card-body conversations">
        </div>
        </div>
        </div>
    </div>
    </div>
@endsection