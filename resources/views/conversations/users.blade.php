<div class="col-md-3 mt-8">
    <div class="list-group">
    @foreach($users as $user)
        <a class="list-group-item" href="{{ route('conversations.show', $user->id) }}"> {{ $user->surname }}</a>
    @endforeach
</div>
</div>