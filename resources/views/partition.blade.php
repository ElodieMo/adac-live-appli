@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Mes partitions</button>
    @if (Auth::user()->roles->pluck('name')->contains('professeur'))
    <button class="mt-8 bg-blue-900 text-white text-xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Ajouter une partition</button>
</div>
    <div class="custom-file">
        <div class="flex flex-col items-center mt-8">
            <form method="POST" action="{{ route('partition.store') }}" enctype="multipart/form-data">
                @csrf
            <button class="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                <span>Download</span>
                <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror" id="validateCustomFile">
                <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z"/></svg>
            </button> 
        </div> 
            <div class="flex flex-col items-center">
        <button type="submit" class="mt-8 bg-blue-900 text-white text-xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default">Ajouter</button>
            </div>
    @endif
            </form>
    </div>
@endsection