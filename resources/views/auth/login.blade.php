 @extends('layouts.app')
    @section('navbar')
    @include('include.navbar')
    @endsection
    @section('content')
<div class="container">
    <div class="flex flex-col items-center">
        <div class="col-md-8">
            <div class="text-center my-5">
                <button class="mt-7 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15 cursor-default" type="button" disabled="disabled">Connexion</button></div>
                <a href="{{route('register')}}" class="flex flex-col items-center hover:no-underline">
                    <button class="flex justify-center mt-7 bg-red-800 text-white text-xl font-bold py-2 px-4 rounded-full w-54 h-15" >
                        Creer un compte
                    </button></a>
                <div class="mt-6 flex justify-center p-3 text-2xl text-black font-extrabold">E-mail</div>
                <form method="POST" action="{{ route('login') }}" class="mx-auto">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="form-label"></label>
                        <div class="col-md-5 mx-auto">
                            <input id="email" placeholder="Adresse e-mail" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="form-label"></label>
                        <div class="col-md-5 mx-auto">
                            <div class="flex justify-center p-8 text-2xl text-black font-extrabold">Mot de passe</div>
                            <input id="password" placeholder="Mot de passe" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="mx-auto m-2">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    Se souvenir de moi
                                </label>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="mx-auto">
                            <button type="submit" class="btn btn-outline-success">
                                Connexion
                            </button>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="mx-auto">
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Mot de passe oublié?
                            </a>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>

@endsection