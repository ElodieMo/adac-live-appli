@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
@include('include.bouton-bonjour')
<div class="flex flex-col items-center">
    <a href="/mon-profil"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Mon profil</button></a></div>
    <div class="flex flex-col items-center">
        <a href="/messagerie"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Messagerie</button></a></div>
        <div class="flex flex-col items-center">
            <a href="#"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-15" type="button">Suivis de cours</button></a></div>   
        <div class="flex flex-col items-center">
            <a href="/mes-documents"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Mes documents</button></a></div>
            <div class="flex flex-col items-center">
                <a href="/les-evenements"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Evènement</button></a>
            </div>
@endsection