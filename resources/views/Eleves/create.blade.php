@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled" name="mon profil">Ajouter mon enfant élève</button>
</div>
<div class="flex flex-col items-center">
                <div class="card-body mt-8">
                    <form method="POST" action="{{ route('Eleves.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid 
                            @enderror" name="name" value="{{ old('name') }}"autocomplete="name" autofocus>
                            
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="surname">Surname</label>
                            <input id="surname" type="text" class="form-control @error('surname') is-invalid 
                            @enderror" name="surname" value="{{ old('surname') }}"autocomplete="surname" autofocus>
                            
                            @error('surname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="ecole">Ecole</label>
                            <input id="ecole" type="text" class="form-control @error('ecole') is-invalid 
                            @enderror" name="ecole" value="{{ old('ecole') }}"autocomplete="ecole" autofocus>
                            
                            @error('ecole')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="Cours">Cours</label>
                            <input id="Cours" type="text" class="form-control @error('Cours') is-invalid 
                            @enderror" name="Cours" value="{{ old('Cours') }}"autocomplete="Cours" autofocus>
                            
                            @error('Cours')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="professeur">Nom du professeur</label>
                            <input id="professeur" type="text" class="form-control @error('professeur') is-invalid 
                            @enderror" name="professeur" value="{{ old('professeur') }}"autocomplete="professeur" autofocus>
                            
                            @error('professeur')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                </div>
                        <div class="flex flex-col items-center">
                                <button type="submit" class="mt-8 bg-blue-900 text-white text-xl font-bold py-1 px-3 rounded-full w-auto h-auto">
                                    Ajouter
                                </button>
                            </form>
                        </div></div>
                        </div>
</div>
@endsection