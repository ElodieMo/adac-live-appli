@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <a href="/bonjour-parent"><button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="button">Parent</button></a></div>
    <div class="flex flex-col items-center">
        <a href="/bonjoureleve"><button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="button">Eleves</button></a>
            </div>
@endsection