@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Mes documents</button></div>
    <div class="flex flex-col items-center">
        <a href="#"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-auto h-15" type="button">Règlement intérieur</button></a></div>
    <div class="flex flex-col items-center">
        <a href="/partitions"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Partitions</button></a>
    <div class="flex flex-col items-center">
        <a href="#"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Autorisation</button></a></div>
            <div class="flex flex-col items-center">
                <a href="#"><button class="mt-12 bg-indigo-200 text-black text-2xl font-bold py-2 px-4 rounded-full w-56 h-15" type="button">Autres</button></a>
            </div>
@endsection