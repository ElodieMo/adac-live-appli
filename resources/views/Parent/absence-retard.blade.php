@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <a href="/mon-profil"><button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Absence / Retard</button></a>
</div>
        <div class="flex flex-col items-center">
            <div class="card-body mt-8">
                        <select class="block appearance-none bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                            <option>Absence</option>
                            <option>Retard</option>
                        </select>

                        @error('Prenom enfant')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>
                <div class="inline-block relative">
                <div class="form-group row">
                    <label for="Prénom enfant" class="col-form-label text-md-right">{{ __("Prénom enfant") }}</label>
    
                        <input id="Prénom enfant" type="text" class="form-control @error('Prénom enfant') is-invalid @enderror" name="Prénom enfant" value="{{ old('Prénom enfant') }}" required autocomplete="Prénom enfant" autofocus>
    
                        @error('Prénom enfant')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="nom enfant" class="col-form-label text-md-right">{{ __("Nom de l'enfant") }}</label>

                        <input id="nom enfant" type="text" class="form-control @error('Nom enfant') is-invalid @enderror" name="nom enfant" value="{{ old('nom enfant') }}" required autocomplete="nom enfant" autofocus>

                        @error('nom enfant')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="Cours concerné" class="col-form-label text-md-right">{{ __("Cours concerné") }}</label>
    
                        <input id="Cours concerné" type="text" class="form-control @error('Cours concerné') is-invalid @enderror" name="Cours concerné" value="{{ old('Cours concerné') }}" required autocomplete="Cours concerné" autofocus>
    
                        @error('Cours concerné')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="Date du cours" class="col-form-label text-md-right">{{ __("Date du cours") }}</label>
        
                        <input id="Date du cours" type="date" class="form-control @error('Date du cours') is-invalid @enderror" name="Date du cours" value="{{ old('Date du cours') }}" required autocomplete="Date du cours" autofocus>
        
                        @error('Date du cours')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="Heure du cours" class="col-form-label text-md-right">{{ __("Heure du cours") }}</label>
            
                        <input id="Heure du cours" type="time" class="form-control @error('Heure du cours') is-invalid @enderror" name="Heure du cours" value="{{ old('Heure du cours') }}" required autocomplete="Heure du cours" autofocus>
            
                        @error('Heure du cours')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <label for="Motif" class="col-form-label text-md-right">{{ __("Motif") }}</label>
                
                        <input id="Motif" type="text" class="form-control @error('Motif') is-invalid @enderror" name="Motif" value="{{ old('Motif') }}" required autocomplete="Motif" autofocus>
                
                        @error('Motif')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                </div>

                <div class="form-group row">
                    <div class="mx-auto mt-8">
                        <button type="submit" class="mt-2 bg-blue-900 text-white text-xl font-bold py-1 px-3 rounded-full w-auto h-auto">
                            Valider
                        </button>

            

    @endsection