@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled">Mes enfants</button>
    <a href="{{route('Eleves.create')}}"><button class="mt-8 bg-blue-900 text-white text-xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text">Ajouter un enfant</button>
<div>
@endsection