@extends('layouts.app')

@section('navbar')
@include('include.navbar')
@endsection

@section('content')
<div class="flex flex-col items-center">
    <button class="mt-8 bg-red-800 text-white text-3xl font-bold py-2 px-4 rounded-full w-auto h-15 cursor-default" type="text" disabled="disabled" name="mon profil">Créer un évènement</button>
</div>
<div class="flex flex-col items-center">
                <div class="card-body mt-8">
                    <form method="POST" action="{{ route('events.store') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="name">Nom</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid 
                            @enderror" name="name" value="{{ old('name') }}"autocomplete="name" autofocus>
                            
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="lieu">Lieu</label>
                            <input id="lieu" type="text" class="form-control @error('lieu') is-invalid 
                            @enderror" name="lieu" value="{{ old('lieu') }}"autocomplete="lieu" autofocus>
                            
                            @error('lieu')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="date_event">Date</label>
                            <input id="date_event" type="date" value="2020-02-15" class="form-control @error('date_event') is-invalid 
                            @enderror" name="date_event" value="{{ old('date_event') }}"autocomplete="date" autofocus>
                            
                            @error('date_event')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="heure_event">heure</label>
                            <input id="heure_event" type="time" value="00:00" class="form-control @error('heure_event') is-invalid 
                            @enderror" name="heure_event" value="{{ old('heure_event') }}"autocomplete="heure_event" autofocus>
                            
                            @error('heure_event')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="caption">Descriptif</label>
                            <input id="caption" type="text" class="form-control @error('caption') is-invalid 
                            @enderror" name="caption" value="{{ old('caption') }}"autocomplete="caption" autofocus>
                            
                            @error('caption')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                            <div class="form-group">
                                <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input @error('image') is-invalid @enderror" id="validateCustomFile">
                                <label class="custom-file-label" for="validateCustomFile">Choisir une image</label>
                                @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                </div>
                            </div>
                </div>
                        <div class="flex flex-col items-center">
                                <button type="submit" class="mt-8 bg-blue-900 text-white text-xl font-bold py-1 px-3 rounded-full w-auto h-auto">
                                    Créer mon évènement
                                </button>
                            </form>
                        </div>
</div>
@endsection
