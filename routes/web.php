<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//routes connexion

Route::get('/bonjour-professeur', function(){
    return view('Professeur.bonjour-professeur');
});

Route::get('/bonjoureleves', function(){
    return view('Eleves.bonjoureleve');
});

//routes messagerie
//fonction visible dans le controlleur conversations nom de la fonction index//
//Route::get('/home', 'ConversationsController@index')->name('home');
//fonction visible dans le controlleur conversations nom de la fonction index//
Route::get('/messagerie', 'ConversationsController@index')->name('messagerie');
//fonction visible dans le controlleur conversations nom de la fonction show//
Route::get('/conversations/{user}', 'ConversationsController@show')->name('conversations.show');


//fin routes messagerie



Route::get('/bonjour-parent', function(){
    return view('Parent.bonjour-parent');
});

Route::get('/mon-planning', function(){
    return view('Professeur.mon-planning');
});

Route::get('/mes-documents-parent', function(){
    return view('Parent.mes-documents-parent');
});

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/home', function () {
    return view('auth/home');
});

//route admin

Route::get('/bonjour-admin', function(){
    return view('admin.users.bonjour-admin');
});

Route::get('/mon-profiladmin', function () {
    return view('admin/users/mon-profiladmin');
});

Route::get('/gestionecoles', function () {
    return view('admin/users/les-ecoles');
});

Route::get('/gestioneleves', function () {
    return view('admin/users/les-eleves');
});

Route::get('/gestioninstruments', function () {
    return view('admin/users/les-instruments');
});

//Route::get('/gestionevenements', function () {
    //return view('admin.users.les-evenements');
//});

//Affichage evenement dans la view les-evenements:
Route::get('/les-evenements', function () {
    $events = App\Event::all();
    return view('admin.users.les-evenements', compact('events'));
});

Route::get('/gestionecoles', function () {
    $ecoles = App\Ecole::all();
    return view('admin.users.les-ecoles', compact('ecoles'));
});

Route::get('/gestioninstruments', function () {
    $instruments = App\Instruments::all();
    return view('admin.users.les-instruments', compact('instruments'));
});

//fin routes admin

//routes commune parent/prof/élèves

Route::get('/mon-profil', function () {
    return view('mon-profil');
});

Route::get('/evenement', function () {
    return view('evenement');
});

Route::get('/mes-documents', function () {
    return view('mes-documents');
});

//fin routes commune

//route eleves

Route::get('/mes-cours', function () {
    return view('mes-cours');
});

Route::get('/bonjoureleve', function () {
    return view('Eleves/bonjoureleve');
});

Route::get('/parenteleve', function () {
    return view('Parent/parenteleve');
});

Route::get('/absence-retard', function () {
    return view('Parent/absence-retard');
});

Route::get('/mes-enfants', function () {
    return view('Parent/enfants-parents');
});

Route::get('/partitions', function () {
    return view('partition');
});

Route::get('/reglement', function () {
    return view('reglement');
});

Route::get('/autres-documents', function () {
    return view('/autre-documents');
});


//fin routes eleves

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/admin/users', 'Admin\UsersController');
Route::resource('/admin/users', 'Admin\EcolesController');


//route gestion des administrateur namespace admin car controller dans le dossier admin
Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
    Route::resource('users', 'UsersController');
});

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
    Route::resource('ecoles', 'ecolesController');
});

//Route::get('event/{event}', 'EventController@edit')->name('event');

//Route::get('/Admin', "Admin\UsersController");

//Disconnected
Route::get('/logout', "Auth\LoginController@logout");

Route::get('/register', function () {
    return view('Auth/register');
});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//redirection part rapport au roles utilisateurs:
Route::get('user/{id}', 'Auth\LoginController@redirectTo');

//formulaire creation evenements:

Route::get('/events/create', 'EventController@create')->name('events.create');
Route::post('/events/destroy/{id}', 'EventController@destroy')->name('events.destroy');
Route::post('/events', 'EventController@store')->name('events.store');

Route::get('/Eleves/create', 'ElevesController@create')->name('Eleves.create');
Route::post('/Eleves/destroy/{id}', 'ElevesController@destroy')->name('Eleves.destroy');
Route::post('/Eleves', 'ElevesController@store')->name('Eleves.store');

Route::post('/partition', 'DocumentController@store')->name('partition.store');

