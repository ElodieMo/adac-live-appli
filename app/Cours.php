<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cours extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function cours()
    {
        return $this->belongsToMany('App\Cours');
    }
}
