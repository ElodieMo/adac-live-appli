<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        //sert à autoriser certain utilisateurs à voir et à gerer les utilisateurs.
        //retourne la view utilisateurs seulement au admin.
        Gate::define('manage-users', function ($user) {
            return $user->HasAnyRole(['admin']);
        });
        //edition de l'utilisateur par l'admin uniquement
        Gate::define('edit-users', function ($user) {
            return $user->isAdmin();
        });
        //suppression de l'utilisateur par l'admin uniquement
        Gate::define('delete-users', function ($user) {
            return $user->isAdmin();
        });

        Gate::define('auth', function ($event) {
            return $user->HasAnyRole(['admin_membre_conseil']);
        });
    }
}
