<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'surname','password','numero_telephone','date_entree',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function conversations()
    {
        return $this->belongsToMany('App\Conversation');
    }

    public function isAdmin()
    {
        return $this->roles()->where('name', 'admin')->first();
    }

    public function HasAnyRole(array $roles)
    {
        return $this->roles()->whereIn('name', $roles)->first();
    }
    public function events()
    {
        return $this->hasMany('App\Event')->orderBy('created_at', 'DESC');
    }

    public function cours()
    {
        return $this->belongsToMany('App\Cour');
    }

    public function instruments()
    {
        return $this->belongsToMany('App\Instrument');
    }

    public function ecoles()
    {
        return $this->belongsToMany('App\Ecole');
    }
}

