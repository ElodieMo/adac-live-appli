<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];
    protected $fillable = [
        'name', 'image', 'lieu','date_event','heure_event','caption',
    ];
    public function users()
    {
        return $this->hasMany('App\User');
    }
}
