<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ecole extends Model
{
    protected $fillable = [
        'name', 'lieu', 'date','heure_debut','heure-fin',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function instruments()
    {
        return $this->belongsToMany('App\Instrument');
    }

    public function cours()
    {
        return $this->belongsToMany('App\Cours');
    }
}
