<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConversationsController extends Controller
{

    
    public function index () {
    $users = User::select('surname', 'id')->where('id','!=', Auth::user()->id)->get();
    return view('conversations/messagerie', compact('users'));
    }

    public function show (User $user)  {
    $users = User::select('surname', 'id')->where('id','!=', Auth::user()->id)->get();
    return view('conversations/show', compact('users', 'user'));
    
    }
}
