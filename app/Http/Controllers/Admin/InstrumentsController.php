<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Instruments;
use Illuminate\Http\Request;

class InstrumentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $Instruments = Instrument::all();
        return view('admin.users.les-instruments')->with('instruments', $instrument);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Instruments  $instruments
     * @return \Illuminate\Http\Response
     */
    public function show(Instruments $instruments)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Instruments  $instruments
     * @return \Illuminate\Http\Response
     */
    public function edit(Instruments $instruments)
    {
        if (Gate::denies('edit-instruments')) {

            return redirect()->route('admin.users.les-instruments');
    }

    $instruments = Instrument::all();

        return view('instruments.edit',[
            'instruments' => $instruments,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Instruments  $instruments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Instruments $instruments)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Instruments  $instruments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Instruments $instruments)
    {
        //
    }
}
