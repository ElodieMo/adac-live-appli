<?php

namespace App\Http\Controllers\Admin;

use App\Ecole;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class EcolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $ecoles = Ecole::all();
        return view('admin.users.les-ecoles')->with('ecoles', $ecole);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ecole  $ecole
     * @return \Illuminate\Http\Response
     */
    public function show(Ecole $ecole)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ecole  $ecole
     * @return \Illuminate\Http\Response
     */
    public function edit(Ecole $ecole)
    {
        if (Gate::denies('edit-ecoles')) {

            return redirect()->route('admin.users.les-ecoles');
    }

    $ecoles = Ecole::all();

        return view('ecoles.edit',[
            'ecoles' => $ecoles,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ecole  $ecole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ecole $ecole)
    {
        $ecole->name = $request->name;
        $ecole->lieu = $request->lieu;
        $ecole->save();

        return redirect()->route('admin.users.les-ecoles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ecole  $ecole
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ecole $ecole)
    {
        if (Gate::denies('delete-ecoles')) {

            return redirect('admin.users.les-ecoles');
    }

    $ecole->delete();

            return redirect()->route('admin.users.les-ecoles');
}
}