<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Gate;
use App\User;
use App\Event;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()
    {
        if (Auth::user()->roles->pluck('name')->contains('admin_membre_conseil'))
    {
        return view ('events.create');
    }

    }

    public function index()
    {
        $events = User::all();
        return view('les-evenements')->with('events', $events);
    }

    public function store()
    {
        $data = request()->validate([
            'name'=> ['required','string'],
            'image'=> ['required','image'],
            'lieu'=> ['required','string'],
            'date_event'=>['nullable','date'],
            'heure_event'=>['nullable','date_format:H:i'],
            'caption'=>['required','string'],
        ]);
        $imagePath = request('image')->store('uploads', 'public');

        //public path permet de pointer vers le dossier public
        $image = Image::make(public_path("/storage/{$imagePath}"))->fit(395, 558);
        $image->save();

        $event= auth()->user()->events()->create([
            'name' => $data['name'],
            'image' => $imagePath,
            'lieu' => $data ['lieu'],
            'date_event'=> $data ['date_event'],
            'heure_event'=> $data ['heure_event'],
            'caption'=> $data ['caption'],
        ])->save();

        return redirect('/les-evenements');// ['user' => auth()->user()]);
    }
    public function edit(Event $event)
        {
            if (Gate::denies('edit-event')) {
    
                return redirect()->route('les-evenements');
            }
    
    
            $events = Event::all();
    
            return view('events.create',[
                'user' => $user,
                'event' => $events,
    
            ]);
        }
        public function update(Request $request, Event $event)
        {
            $event->name = $request->name;
            $event->image = $request->name;
            $event->lieu = $request->lieu;
            $event->date_event = $request->date_event;
            $event->heure_event = $request->heure_event;
            $event->caption = $request->caption;
            $event->save();
    
            return redirect()->route('les-evenements');
        }

        /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */

        public function destroy($id)
        {
            // if (Gate::denies('delete-event')) {
    
            //     return redirect('gestionevenements');
            // }
    
            $event = Event::find($id);
            $event->delete();
    
            return redirect('les-evenements');
        }
    }