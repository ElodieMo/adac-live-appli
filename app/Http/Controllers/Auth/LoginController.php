<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home scre
    en. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo()
    {
        //Si mon nom de role contient le role admin retourner cette vue,
        if (Auth::user()->roles->pluck('name')->contains('admin')){
            return'/bonjour-admin';
            //Si mon nom de role contient le role professeur retourner cette vue,
        } elseif (Auth::user()->roles->pluck('name')->contains('admin_membre_conseil')){
            return '/bonjour-professeur';
         //Si mon nom de role contient le role professeur retourner cette vue,
        } elseif (Auth::user()->roles->pluck('name')->contains('professeur')){
            return '/bonjour-professeur';
         //Si mon nom de role contient le role parent retourner cette vue,
        } elseif (Auth::user()->roles->pluck('name')->contains('parent')){
        return '/parenteleve';
         //Si c'est un autre role, retourner cette vue,
        } else {
            return '/bonjoureleve';
        }
    }
}

