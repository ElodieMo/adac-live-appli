<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class DocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create()
    {
        if (Auth::user()->roles->pluck('name')->contains('professeur'))
    {
        return view ('document.create');
    }

    }

    public function index()
    {
        // $events = User::all();
        // return view('reglement')->with('events', $events);
    }

    public function store()
    {
        $imagePath = request('image')->store('uploads', 'public');

        $image = Image::make(public_path("/storage/{$imagePath}"))->fit(395, 558);
        $image->save();
    }
}
